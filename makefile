results: start
	rm -f *.mod

#! echo 'make rk - Метод Рунге-Кутты'
#! echo 'make ae - Экстраполяционный метод Адамса'
#! echo 'make ai - Интерполяционный метод Адамса'


start: params.f90 rk_ea_ia.f90 Newton_module.f90 Integral.f90 VP04.f90 VP09.f90
	gfortran $^ -o start

rk: start
	./start rk

ae: start
	./start ae

ai: start
	./start ai
