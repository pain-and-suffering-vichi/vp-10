﻿module basov
    
    use params
    use ODU_solver

    use Newton_module
    implicit none
    
    contains
    
    subroutine Write_to_file(file, T, X)
    real(8), dimension(0:, 1:), intent(in) :: X
    real(8), dimension(0:), intent(in) :: T
    character(10), intent(in) :: file
    
        open(10, file = file)
            do i = 0, int(interval_end/h) + 1
                write(10,*) t(i), X(i,:)
            enddo
        close(10)
        
    end subroutine Write_to_file
    
    subroutine Rosen(X)
    real(8), parameter :: aplpha = 1.077, beta = -0.372, gamma = -0.577
    real(8), dimension(0:, 1:), intent(out) :: X
    real(8), dimension(1:ndim, 1:ndim) :: J, E
    real(8), dimension(1:ndim) :: XXh
    real(8), dimension(0:size(X)/ndim-1) :: T
    real(8), dimension(1:ndim+1,1:ndim) :: Extended
    integer(4) :: i
    character(10) :: file
    
        forall(i=1:ndim) E(i,i) = 1
       
        T = 0; X(0,:) = X_started
        do i=0, size(x)/ndim - 2
            call yakobian(X(i,:), J, f)
            
            Extended(1:ndim,:) = E - aplpha*h*J - beta*h*h*matmul(J,J)
            Extended(ndim+1,:) = f( t(i), X(i,:) + gamma*h*f(t(i),X(i,:)) )
            call choise(Extended, XXh, ndim)
            
            X(i+1,:) = h*XXh + X(i,:)
            
            T(i+1) = h*(i+1)
        enddo
    
        file = 'rosen.dat'
        call Write_to_file(file, T, X)
    end subroutine Rosen
    
    subroutine yakobian(X0, df, f)
    real(8), dimension(1:), intent(in) :: X0
    real(8), dimension(1:size(X0), 1:size(X0)), intent(out) :: df
    real(8), dimension(1:size(X0)) :: X
    real(8) :: eps=2e-8
    integer(8) :: i, n
    interface 
        function f(t,X) result (Y)
        real(8), dimension(1:), intent(in) :: X
        real(8), intent(in) :: t
        real(8), dimension(1:size(X)) :: Y
        integer(4) :: i
        end function f
    end interface
    
    n=size(X0)
    
    do i=1,n
        X = X0
        X(i) = X0(i)+eps
        df(i,1:n) = (f(0.0_8,X)-f(0.0_8,X0))/eps
    enddo
    end subroutine yakobian
    
    subroutine Precor(X)
    real(8), dimension(0:, 1:), intent(out) :: X
    real(8), dimension(0:size(X)/ndim-1) :: T
    integer(4) :: i, j
    character(10) :: file
    real(8), dimension(0:n_extr_adams-1) :: A
    real(8), dimension(1:ndim) :: sum
    real(8), dimension(-1:n_inter_adams-2) :: B
    
        T = 0; X(0,:) = X_started
        do i = 0, n_extr_adams-2
            X(i+1,:) = rk(X(i,:), t(i))
            T(i+1) = h*(i+1)
        enddo
        
        A = A_adams(n_extr_adams,(/(i,i=0,n_extr_adams-1)/))
        B = B_adams(n_inter_adams,(/(i,i=-1,n_inter_adams-2)/))
        
        
        do i = n_extr_adams-1, size(x)/ndim - 2
            call ea(X, T, A, i)
            call ia(X, T, B, i)
        enddo
        
        file = 'precor.dat'
        call Write_to_file(file, T, X)
        
        contains

            function f_for_newton(z)
            real(8), dimension(1:), intent(in) :: z
            real(8), dimension(1:size(z)) :: f_for_newton

                f_for_newton = X(i-1,:) + h*sum - z + h*B(-1)*f(T(i),z)

            end function f_for_newton
    
    end subroutine Precor
    
end module basov
    